
# Test Users CRUD

import os
import pytest

from selenium import webdriver
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.webdriver import WebDriver

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from dataclasses import dataclass
from faker import Faker

from dotenv import load_dotenv

from base import *
from utils import *


class TestUsers(ReboticsTestCase):
    def test_creation(self, driver: WebDriver):
        self.auth(driver)

        change_admin_page(driver, AdminPage.ASSOCIATE)
        btn = get_create_btn(driver, AdminPage.ASSOCIATE)
        assert btn

        btn.click()
        wait_for_form(driver)

        password = fake.bothify('?#######?')
        form_input_text(driver, 'username', fake.profile()['username'])
        form_input_text(driver, 'password', password)
        form_input_text(driver, 'repeat password', password)
        form_input_text(driver, 'email', fake.bothify('test###??') + '@test.com')

        save_btn = get_save_btn(driver)
        assert save_btn

        save_btn.click()
        wait_for_edit_btn(driver)

    def test_edit(self, driver: WebDriver):
        self.auth(driver)

        change_admin_page(driver, AdminPage.ASSOCIATE)
        first_user = driver.find_elements(By.CSS_SELECTOR, 'td')[1]
        first_user.click()

        wait_for_edit_btn(driver)
        edit_btn = get_edit_btn(driver)
        assert edit_btn

        edit_btn.click()

        form_input_text(driver, 'Custom ID', fake.bothify('Test ##??'))
        save_btn = get_save_btn(driver)

        assert save_btn

        save_btn.click()
        wait_for_edit_btn(driver)
