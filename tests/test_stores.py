# Test Stores CRUD

import os
import pytest

from selenium import webdriver
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.webdriver import WebDriver

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from dataclasses import dataclass
from faker import Faker

from dotenv import load_dotenv

from base import *
from utils import *


class TestStores(ReboticsTestCase):
    STORE_ADMIN_URL = f'{URL}/administration/store'

    def test_creation(self, driver: WebDriver):
        self.auth(driver)

        new_button = get_create_btn(driver, AdminPage.STORE)
        assert new_button

        new_button.click()

        wait_for_form(driver)

        form_input_text(driver, 'Store Name', 'Store of ' + fake.name())
        form_input_text(driver, 'Store ID', fake.bothify('Store???#####'))

        form_input_select(driver, 'Country', '1')
        form_input_select(driver, 'State', '0007343500004 - KGHW HAWAIIAN ROLLS 12CT')
        form_input_select(driver, 'City', '2023-01-12 6:13:14')

        form_input_text(driver, 'Address Line', fake.address()[:90])
        form_input_text(driver, 'Email', 'test@test.com')

        save_button = get_save_btn(driver)
        assert save_button

        save_button.click()

        @check_func
        def check_edit(driver: WebDriver):
            elements = driver.find_elements(By.CSS_SELECTOR, 'span')
            for element in elements:
                if 'edit' in element.text.strip().lower():
                    return True

            return False

        wait = WebDriverWait(driver, 50)
        wait.until(check_edit)

    def test_edit(self, driver: WebDriver):
        self.auth(driver)

        first_store = driver.find_elements(By.CSS_SELECTOR, 'td')[1]
        first_store.click()

        def get_edit_button(driver: WebDriver):
            elements = driver.find_elements(By.CSS_SELECTOR, 'span')
            for element in elements:
                if 'edit' in element.text.strip().lower():
                    return element

        @check_func
        def check_edit(driver: WebDriver):
            edit_button = get_edit_button(driver)
            return bool(edit_button)

        wait = WebDriverWait(driver, 50)
        wait.until(check_edit)

        edit_button = get_edit_button(driver)
        assert edit_button

        edit_button.click()

        form_input_text(driver, 'Store Name', 'Store of ' + fake.name())
        form_input_text(driver, 'Address Line', fake.address()[:90])

        save_button = get_save_btn(driver)
        assert save_button

        save_button.click()
