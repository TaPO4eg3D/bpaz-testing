
# Test Users CRUD

import os
import pytest

from selenium import webdriver
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.webdriver import WebDriver

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from dataclasses import dataclass
from faker import Faker

from dotenv import load_dotenv

from base import *
from utils import *


class TestGroups(ReboticsTestCase):
    def test_creation(self, driver: WebDriver):
        self.auth(driver)

        change_admin_page(driver, AdminPage.USER_GROUPS)
        btn = get_create_btn(driver, AdminPage.USER_GROUPS)
        assert btn

        btn.click()
        wait_for_form(driver)

        group_name = fake.bothify('TestGroup##??')
        form_input_text(driver, 'user group name', group_name)

        save_btn = get_save_btn(driver)
        assert save_btn

        save_btn.click()
        wait_for_edit_btn(driver)

    def test_edit(self, driver: WebDriver):
        self.auth(driver)

        driver.get(URL + '/administration/user-groups/18')

        wait_for_edit_btn(driver)
        edit_btn = get_edit_btn(driver)
        assert edit_btn

        edit_btn.click()

        group_name = fake.bothify('TestGroup##??')
        form_input_text(driver, 'user group name', group_name, clear = True)

        save_btn = get_save_btn(driver)

        assert save_btn

        save_btn.click()
        wait_for_edit_btn(driver)
