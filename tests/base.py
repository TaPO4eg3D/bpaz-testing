
import os
import pytest

from selenium import webdriver
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.webdriver import WebDriver

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from dataclasses import dataclass
from faker import Faker

from dotenv import load_dotenv
from utils import *

load_dotenv()

URL = 'http://epsilon.rebotics.net'
fake = Faker()


@pytest.fixture()
def driver():
    service = Service('./geckodriver')
    driver = webdriver.Firefox(service=service)
    driver.implicitly_wait(20)

    yield driver

    driver.close()


class ReboticsTestCase:
    driver: WebDriver

    def auth(self, driver: WebDriver):
        driver.get(f'{URL}/log-in')

        text_inputs = driver.find_elements(By.CSS_SELECTOR, ".form-group input")

        text_inputs[0].send_keys(os.getenv('USERNAME'))
        text_inputs[1].send_keys(os.getenv('PASSWORD'))

        login_button = driver.find_element(By.CSS_SELECTOR, 'button[type="submit"]')
        login_button.click()

        wait_for_admin_page(driver, AdminPage.STORE)


