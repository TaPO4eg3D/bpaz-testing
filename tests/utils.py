import time

from enum import Enum

from selenium import webdriver
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.webdriver import WebDriver

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from functools import wraps

class AdminPage(str, Enum):
    STORE = 'store'
    ASSOCIATE = 'associate'
    USER_GROUPS = 'user groups'
    FILE = 'file'
    ASSORTMENT = 'assortment'
    PRODUCT = 'product'
    SUPPLIER = 'supplier'

    def check_button(self, btn_text: str) -> bool:
        def _check(val: str):
            return val in btn_text.strip().lower()

        if self == AdminPage.STORE:
            return _check('new store')

        if self == AdminPage.ASSOCIATE:
            return _check('new user')

        if self == AdminPage.USER_GROUPS:
            return _check('new user group')

        if self == AdminPage.FILE:
            return _check('new file')

        if self == AdminPage.ASSORTMENT:
            return _check('new assortment')

        if self == AdminPage.PRODUCT:
            return _check('new product')

        if self == AdminPage.SUPPLIER:
            return _check('new supplier')

        return False


def check_func(func):
    @wraps(func)
    def wrapper(*args, **kwrags):
        try:
            return func(*args, **kwrags)
        except Exception as e:
            print(e)

    return wrapper


def get_save_btn(driver: WebDriver):
    buttons = driver.find_elements(By.CSS_SELECTOR, 'button')
    for button in buttons:
        if 'save' in button.text.strip().lower():
            return button

    return None


def get_create_btn(driver: WebDriver, page: AdminPage):
    elements = driver.find_elements(By.CSS_SELECTOR, 'button')

    for element in elements:
        if page.check_button(element.text):
            return element

    return None


def wait_for_admin_page(driver: WebDriver, page: AdminPage):
    @check_func
    def _check(driver: WebDriver) -> bool:
        elements = driver.find_elements(By.CSS_SELECTOR, 'button')

        for element in elements:
            if page.check_button(element.text):
                return True

        return False

    wait = WebDriverWait(driver, 50)
    wait.until(_check)


def wait_for_edit_btn(driver: WebDriver):
    wait = WebDriverWait(driver, 50)
    wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, 'button.button-edit')))


def get_edit_btn(driver: WebDriver):
    return driver.find_element(By.CSS_SELECTOR, 'button.button-edit')


def wait_for_form(driver: WebDriver):
    @check_func
    def _check(driver: WebDriver):
        buttons = driver.find_elements(By.CSS_SELECTOR, 'button')
        for button in buttons:
            if 'save' in button.text.strip().lower():
                return True

        return False

    wait = WebDriverWait(driver, 50)
    wait.until(_check)


def form_input_text(driver: WebDriver, field_name: str, value: str, clear = False):
    labels = driver.find_elements(By.CSS_SELECTOR, 'label')

    for label in labels:
        if field_name.lower() in label.text.strip().lower():
            parent = label.find_element(By.XPATH, '..')
            element = parent.find_element(By.CSS_SELECTOR, 'input')

            if clear:
                element.send_keys(Keys.CONTROL, 'a')
                element.send_keys(Keys.BACKSPACE)

            element.send_keys(value)

            return

    raise RuntimeError(f'Unable to Find Form Field with name {field_name}')


def change_admin_page(driver: WebDriver, page: AdminPage):
    expand_btn = driver.find_element(By.CSS_SELECTOR, 'button.button-toggle')
    expand_btn.click()
    
    tab_buttons = driver.find_elements(By.CSS_SELECTOR, 'a.tab-button')
    for btn in tab_buttons:
        if page in btn.text.strip().lower():
            btn.click()
            break

    wait_for_admin_page(driver, page)


def form_input_select(driver: WebDriver, field_name: str, option: str):
    labels = driver.find_elements(By.CSS_SELECTOR, 'label')

    element = None
    for label in labels:
        if field_name.lower() in label.text.strip().lower():
            element = label.find_element(By.XPATH, '..')
            break

    if not element:
        raise RuntimeError(f'Unable to Find Form Field with name {field_name}')

    btn = element.find_element(By.CSS_SELECTOR, 'button')
    btn.click()

    time.sleep(1)

    buttons = driver.find_elements(By.CSS_SELECTOR, 'button.options-list-redesign__option')

    for button in buttons:
        if option.lower() in button.text.strip().lower():
            button.click()
            return
